/*
 * Copyright (C) 1999-2010  Terence M. Welsh
 *
 * This file is part of Rgbhsl.
 *
 * Rgbhsl is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rgbhsl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


// This library converts between colors defined with RGB values and HSL
// values.  It also finds in-between colors by moving linearly through
// HSL space.
// All functions take values for r, g, b, h, s, and l between 0.0 and 1.0
// (RGB = red, green, blue;  HSL = hue, saturation, luminosity)

#ifndef RGBHSL_H
#define RGBHSL_H


// convert RGB to HSL

void rgb2hsl(float r, float g, float b, float &h, float &s, float &l);

inline void rgb2hsl(const float* rgb, float *hsl) {
	rgb2hsl(rgb[0], rgb[1], rgb[2], hsl[0], hsl[1], hsl[2]);
}


// convert HSL to RGB
// 0 <= {s, l} <= 1
// hue is cyclical. 0 to 1 represents one entire hue cycle, red to red.

void hsl2rgb(float h, float s, float l, float &r, float &g, float &b);

inline void hsl2rgb(const float* hsl, float *rgb) {
	hsl2rgb(hsl[0], hsl[1], hsl[2], rgb[0], rgb[1], rgb[2]);
}

inline void hsl2rgb(float h, float s, float l, float *rgb) {
	hsl2rgb(h, s, l, rgb[0], rgb[1], rgb[2]);
}

// For these 'tween functions, a tween value of 0.0 will output the first
// color while a tween value of 1.0 will output the second color.
// A value of 0 for direction indicates a positive progression around
// the color wheel (i.e. red -> yellow -> green -> cyan...).  A value of
// 1 does the opposite.
void hslTween(float h1, float s1, float l1,
	float h2, float s2, float l2, float tween, int direction,
	float &outh, float &outs, float &outl);

inline void hslTween(const float* hsl1, const float* hsl2,
	float tween, int direction,
	float *hslOut) {
	hslTween(hsl1[0], hsl1[1], hsl1[2], hsl2[0], hsl2[1], hsl2[2], tween, direction, hslOut[0], hslOut[1], hslOut[2]);
}

void rgbTween(float r1, float g1, float b1,
	float r2, float g2, float b2, float tween, int direction,
	float &outr, float &outg, float &outb);

void rgbTween(const float* rgb1, const float* rgb2, float tween, int direction, float *rgb);


#endif // RGBHSL_H
