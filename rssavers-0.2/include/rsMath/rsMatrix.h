/*
 * Copyright (C) 1999-2010  Terence M. Welsh
 *
 * This file is part of rsMath.
 *
 * rsMath is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * rsMath is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef RSMATRIX_H
#define RSMATRIX_H



#include <ostream>



class rsVec;
class rsQuat;



class rsMatrix {
public:
	float m[16];
	// 1 0 0 x   0 4 8  12  <-- Column order matrix just like OpenGL
	// 0 1 0 y   1 5 9  13
	// 0 0 1 z   2 6 10 14
	// 0 0 0 1   3 7 11 15

	static const float id[16];

	rsMatrix() { }
	rsMatrix(const float* mData) { this->set(mData); }
	~rsMatrix() { }

	// cast to array
	operator float* () { return m; }
	operator const float* () const { return m; }
	// reference
	float & operator [] (int i) { return m[i]; }
	const float & operator [] (int i) const { return m[i]; }
	
	// setters
	inline rsMatrix & operator = (const rsMatrix &mat) { set(mat); return *this; }
	inline void copy(const rsMatrix &mat) { set(mat); }
	void set(const float* mat) {
		memcpy(m, mat, 16 * sizeof(float));
	}

	// getters
	void get(float* mat) const {
		memcpy(mat, m, 16 * sizeof(float));
	}
	void getTranslate(float v[3]) const { v[0] = m[12]; v[1] = m[13]; v[2] = m[14]; }

	// reference getters
	float* get() { return m; }
	const float* col0() const { return &m[ 0]; }
	const float* col1() const { return &m[ 4]; }
	const float* col2() const { return &m[ 8]; }
	const float* col3() const { return &m[12]; }
	const float* getTranslate() const { return &m[12]; }

	// matrix math

	void identity() { set(rsMatrix::id); }

	void preMult(const rsMatrix &postMat);
	void postMult(const rsMatrix &preMat);
	void mult(const rsMatrix &l, const rsMatrix &r);

	void makeTranslate(float x, float y, float z);
	void makeTranslate(const float* p);
	void makeScale(float s);
	void makeScale(float x, float y, float z);
	void makeScale(const float* s);
	void makeRotate(float a, float x, float y, float z);  // normalized angle, axis
	void makeRotate(float a, const float v[3]);  // normalized angle, axis
	void makeRotate(const rsQuat &q);

	void translate(float x, float y, float z);
	void translate(const float* p);
	void translate(const rsVec &vec);
	void scale(float s);
	void scale(float x, float y, float z);
	void scale(const float* s);
	void scale(const rsVec &vec);
	void rotate(float a, float x, float y, float z);  // normalized angle, axis
	void rotate(float a, const rsVec &v);  // normalized angle, axis
	void rotate(const rsQuat &q);

	void fromQuat(const rsQuat &q);

	// general matrix inversion
	float determinant() const;
	bool invert() { rsMatrix mOld(*this); this->invert(mOld); }
	bool invert(const rsMatrix &mat);
	void rotationInvert(const rsMatrix &mat);  // inversion of 3x3 submatrix
	void transpose(const rsMatrix &mat);

	// 3x3 matrix fns
	rsVec transPoint(const rsVec &m) const;
	rsVec transVec(const rsVec &m) const;

	std::ostream & operator << (std::ostream &os) const;
};

// templated util fns

template <class T>
T determinant3(
	const T aa, const T ab, const T ac,
	const T ba, const T bb, const T bc,
	const T ca, const T cb, const T cc) {
	return (aa * bb * cc) + (ab * bc * ca) + (ac * ba * cb)
		- (aa * bc * cb) - (ab * ba * cc) - (ac * bb * ca);
}

template <class T>
T determinant4(const T mat[16]) {
	const T aa(mat[0]);
	const T ba(mat[1]);
	const T ca(mat[2]);
	const T da(mat[3]);
	const T ab(mat[4]);
	const T bb(mat[5]);
	const T cb(mat[6]);
	const T db(mat[7]);
	const T ac(mat[8]);
	const T bc(mat[9]);
	const T cc(mat[10]);
	const T dc(mat[11]);
	const T ad(mat[12]);
	const T bd(mat[13]);
	const T cd(mat[14]);
	const T dd(mat[15]);

	const T det3_1( determinant3(bb, bc, bd, cb, cc, cd, db, dc, dd));
	const T det3_2(-determinant3(ab, ac, ad, cb, cc, cd, db, dc, dd));
	const T det3_3( determinant3(ab, ac, ad, bb, bc, bd, db, dc, dd));
	const T det3_4(-determinant3(ab, ac, ad, bb, bc, bd, cb, cc, cd));

	return (
		  mat[0] * det3_1 
		+ mat[1] * det3_2 
		+ mat[2] * det3_3 
		+ mat[3] * det3_4);
}

template <class T>
bool invertMatrix(const T mat[16], T invmat[16]) {
	const T aa(mat[0]);
	const T ba(mat[1]);
	const T ca(mat[2]);
	const T da(mat[3]);
	const T ab(mat[4]);
	const T bb(mat[5]);
	const T cb(mat[6]);
	const T db(mat[7]);
	const T ac(mat[8]);
	const T bc(mat[9]);
	const T cc(mat[10]);
	const T dc(mat[11]);
	const T ad(mat[12]);
	const T bd(mat[13]);
	const T cd(mat[14]);
	const T dd(mat[15]);

	// calculate determinant
	const T det3_1( determinant3(bb, bc, bd, cb, cc, cd, db, dc, dd));
	const T det3_2(-determinant3(ab, ac, ad, cb, cc, cd, db, dc, dd));
	const T det3_3( determinant3(ab, ac, ad, bb, bc, bd, db, dc, dd));
	const T det3_4(-determinant3(ab, ac, ad, bb, bc, bd, cb, cc, cd));
	const T det(
		  mat[0] * det3_1
		+ mat[1] * det3_2
		+ mat[2] * det3_3
		+ mat[3] * det3_4);


	// reciprocal of determinant
	const T rec_det((T)(1) / det);

	if(!isfinite(rec_det)) {
		// matrix is singular, cannot be inverted
		return false;
	}

	// calculate inverted matrix
	invmat[0 ] = det3_1 * rec_det;
	invmat[4 ] = det3_2 * rec_det;
	invmat[8 ] = det3_3 * rec_det;
	invmat[12] = det3_4 * rec_det;
	invmat[1 ] = -determinant3(ba, bc, bd, ca, cc, cd, da, dc, dd) * rec_det;
	invmat[5 ] =  determinant3(aa, ac, ad, ca, cc, cd, da, dc, dd) * rec_det;
	invmat[9 ] = -determinant3(aa, ac, ad, ba, bc, bd, da, dc, dd) * rec_det;
	invmat[13] =  determinant3(aa, ac, ad, ba, bc, bd, ca, cc, cd) * rec_det;
	invmat[2 ] =  determinant3(ba, bb, bd, ca, cb, cd, da, db, dd) * rec_det;
	invmat[6 ] = -determinant3(aa, ab, ad, ca, cb, cd, da, db, dd) * rec_det;
	invmat[10] =  determinant3(aa, ab, ad, ba, bb, bd, da, db, dd) * rec_det;
	invmat[14] = -determinant3(aa, ab, ad, ba, bb, bd, ca, cb, cd) * rec_det;
	invmat[3 ] = -determinant3(ba, bb, bc, ca, cb, cc, da, db, dc) * rec_det;
	invmat[7 ] =  determinant3(aa, ab, ac, ca, cb, cc, da, db, dc) * rec_det;
	invmat[11] = -determinant3(aa, ab, ac, ba, bb, bc, da, db, dc) * rec_det;
	invmat[15] =  determinant3(aa, ab, ac, ba, bb, bc, ca, cb, cc) * rec_det;

	return true;
}


#endif
