/*
 * Copyright (C) 1999-2010  Terence M. Welsh
 *
 * This file is part of rsMath.
 *
 * rsMath is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * rsMath is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "rsMath.h"

#ifndef RSVEC_H
#define RSVEC_H



class rsMatrix;



class rsVec {
public:
	float v[3];

	rsVec() { set(0, 0, 0); }
	rsVec(float xx, float yy, float zz) { set(xx, yy, zz); }
	rsVec(const float* xyz) { set(xyz[0], xyz[1], xyz[2]); }
	virtual ~rsVec() { }

	float & operator [] (int i) { return v[i]; }
	const float & operator [] (int i) const { return v[i]; }

	void set(float xx = 0, float yy = 0, float zz = 0) {
		v[0] = xx;
		v[1] = yy;
		v[2] = zz;
	}

	// cast to array
	operator float* () { return v; }
	operator const float* () const { return v; }

	float length() const;
	float lengthsq() const;
	float normalize();
	float dot(const rsVec &v) const;
	friend float dot(const rsVec &a, const rsVec &b);
	void cross(const rsVec&, const rsVec&);
	void scale(float);
	void transPoint(const rsMatrix &m);
	void transVec(const rsMatrix &m);
	int almostEqual(const rsVec& vec, float tolerance) const;
	rsVec sign() const {
		return rsVec((float)sgn(v[0]), (float)sgn(v[1]), (float)sgn(v[2]));
	}
	rsVec abs() const {
		return rsVec(fabsf(v[0]), fabsf(v[1]), fabsf(v[2]));
	}

	// unary operators

	rsVec operator - () {
		return rsVec(-v[0], -v[1], -v[2]);
	}

	// vector-scalar operators

	rsVec operator + (float f) const
		{ return(rsVec(v[0] + f, v[1] + f, v[2] + f)); }
	rsVec & operator += (float d)
		{ v[0] += d; v[1] += d; v[2] += d; return *this; }
	rsVec operator - (float f) const
		{ return(rsVec(v[0] - f, v[1] - f, v[2] - f)); }
	rsVec & operator -= (float d)
		{ v[0] -= d; v[1] -= d; v[2] -= d; return *this; }

	rsVec operator * (float mul) const
		{ return(rsVec(v[0]*mul, v[1]*mul, v[2]*mul)); }
	rsVec & operator *= (float mul)
		{ v[0] *= mul; v[1] *= mul; v[2] *= mul; return *this; }
	rsVec operator / (float div) const
		{float rec = 1.0f/div; return(rsVec(v[0]*rec, v[1]*rec, v[2]*rec));}
	rsVec & operator /= (float div)
		{ v[0] /= div; v[1] /= div; v[2] /= div; return *this; }

	// float-vector operators

	friend rsVec operator + (float f, const rsVec &v) {
		return rsVec(f + v[0], f + v[1], f + v[2]);
	}
	friend rsVec operator - (float f, const rsVec &v) {
		return rsVec(f - v[0], f - v[1], f - v[2]);
	}
	friend rsVec operator * (float f, const rsVec &v) {
		return rsVec(f * v[0], f * v[1], f * v[2]);
	}
	friend rsVec operator / (float f, const rsVec &v) {
		return rsVec(f / v[0], f / v[1], f / v[2]);
	}

	// vector - vector operators

	// addition/substraction
	rsVec operator + (const rsVec &vec) const
		{return(rsVec(v[0] + vec[0], v[1] + vec[1], v[2] + vec[2]));}
	rsVec & operator += (const rsVec &vec)
		{v[0]+=vec[0]; v[1]+=vec[1]; v[2]+=vec[2]; return *this;}
	rsVec operator - (const rsVec &vec) const
		{return(rsVec(v[0] - vec[0], v[1] - vec[1], v[2] - vec[2]));}
	rsVec & operator -= (const rsVec &vec)
		{v[0]-=vec[0]; v[1]-=vec[1]; v[2]-=vec[2]; return *this;}

	// element-by-element multiplication
	rsVec operator * (const rsVec &mul) const
		{ return(rsVec(v[0] * mul.v[0], v[1] * mul.v[1], v[2] * mul.v[2])); }
	rsVec & operator *= (const rsVec &vec)
		{v[0]*=vec[0]; v[1]*=vec[1]; v[2]*=vec[2]; return *this;}

	// random vector with 0 <= {x, y, z} < f
	static rsVec rsRand(float f) {
		return rsVec(rsRandf(f), rsRandf(f), rsRandf(f));
	}

	// random vector with 0 <= {x, y, z} < 1
	static rsVec rsRand() {
		return rsVec(rsRandf(), rsRandf(), rsRandf());
	}

	void clamp(float minValue, float maxValue) {
		for (int i = 0; i < 3; ++i) {
			v[i] = (v[i] < minValue ? minValue : (v[i] > maxValue ? maxValue : v[i]));
		}
	}

	// sets value to interpolate between b and c
	void interpolate(const rsVec& a, const rsVec& b, const rsVec& c, const rsVec& d, float where);
};

float interpolate(float a, float b, float c, float d, float where);


#endif
