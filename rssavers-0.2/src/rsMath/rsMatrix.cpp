/*
 * Copyright (C) 1999-2010  Terence M. Welsh
 *
 * This file is part of rsMath.
 *
 * rsMath is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * rsMath is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <rsMath/rsMath.h>
#include <rsMath/rsMatrix.h>
#include <rsMath/rsVec.h>
#include <rsMath/rsQuat.h>
#include <math.h>



const float rsMatrix::id[16] = { 
	1,0,0,0, 
	0,1,0,0, 
	0,0,1,0, 
	0,0,0,1 };


void rsMatrix::mult(const rsMatrix &l, const rsMatrix &r) {
	m[ 0] = l[ 0] * r[ 0] + l[ 4] * r[ 1] + l[ 8] * r[ 2] + l[12] * r[ 3];
	m[ 1] = l[ 1] * r[ 0] + l[ 5] * r[ 1] + l[ 9] * r[ 2] + l[13] * r[ 3];
	m[ 2] = l[ 2] * r[ 0] + l[ 6] * r[ 1] + l[10] * r[ 2] + l[14] * r[ 3];
	m[ 3] = l[ 3] * r[ 0] + l[ 7] * r[ 1] + l[11] * r[ 2] + l[15] * r[ 3];
	m[ 4] = l[ 0] * r[ 4] + l[ 4] * r[ 5] + l[ 8] * r[ 6] + l[12] * r[ 7];
	m[ 5] = l[ 1] * r[ 4] + l[ 5] * r[ 5] + l[ 9] * r[ 6] + l[13] * r[ 7];
	m[ 6] = l[ 2] * r[ 4] + l[ 6] * r[ 5] + l[10] * r[ 6] + l[14] * r[ 7];
	m[ 7] = l[ 3] * r[ 4] + l[ 7] * r[ 5] + l[11] * r[ 6] + l[15] * r[ 7];
	m[ 8] = l[ 0] * r[ 8] + l[ 4] * r[ 9] + l[ 8] * r[10] + l[12] * r[11];
	m[ 9] = l[ 1] * r[ 8] + l[ 5] * r[ 9] + l[ 9] * r[10] + l[13] * r[11];
	m[10] = l[ 2] * r[ 8] + l[ 6] * r[ 9] + l[10] * r[10] + l[14] * r[11];
	m[11] = l[ 3] * r[ 8] + l[ 7] * r[ 9] + l[11] * r[10] + l[15] * r[11];
	m[12] = l[ 0] * r[12] + l[ 4] * r[13] + l[ 8] * r[14] + l[12] * r[15];
	m[13] = l[ 1] * r[12] + l[ 5] * r[13] + l[ 9] * r[14] + l[13] * r[15];
	m[14] = l[ 2] * r[12] + l[ 6] * r[13] + l[10] * r[14] + l[14] * r[15];
	m[15] = l[ 3] * r[12] + l[ 7] * r[13] + l[11] * r[14] + l[15] * r[15];
}

void rsMatrix::preMult(const rsMatrix &postMat) {
	rsMatrix preMat(*this);
	this->mult(preMat, postMat);
}


void rsMatrix::postMult(const rsMatrix &preMat){
	rsMatrix postMat(*this);
	this->mult(preMat, postMat);
}


void rsMatrix::makeTranslate(float x, float y, float z){
	set(rsMatrix::id);
	m[12] = x;
	m[13] = y;
	m[14] = z;
}

void rsMatrix::makeTranslate(const float *p) {
	set(rsMatrix::id);
	m[12] = p[0];
	m[13] = p[1];
	m[14] = p[2];
}


void rsMatrix::makeScale(float s) {
	set(rsMatrix::id);
	m[0] = s;
	m[5] = s;
	m[10] = s;
}

void rsMatrix::makeScale(float x, float y, float z) {
	set(rsMatrix::id);
	m[0] = x;
	m[5] = y;
	m[10] = z;
}

void rsMatrix::makeScale(const float* s) {
	set(rsMatrix::id);
	m[0] = s[0];
	m[5] = s[1];
	m[10] = s[2];
}


void rsMatrix::makeRotate(float a, float x, float y, float z){
	rsQuat q;
	q.make(a, x, y, z);
	q.toMat(m);
}

void rsMatrix::makeRotate(float a, const float v[3]) {
	rsQuat q;
	q.make(a, v);
	q.toMat(m);
}

void rsMatrix::makeRotate(const rsQuat &q) {
	q.toMat(m);
}


void rsMatrix::translate(float x, float y, float z) {
	rsMatrix mat;
	mat.makeTranslate(x, y, z);
	this->postMult(mat);
}

void rsMatrix::translate(const float *p) {
	rsMatrix mat;
	mat.makeTranslate(p);
	this->postMult(mat);
}

void rsMatrix::translate(const rsVec &vec) {
	rsMatrix mat;
	mat.makeTranslate(vec);
	this->postMult(mat);
}


void rsMatrix::scale(float s) {
	rsMatrix mat;
	mat.makeScale(s);
	this->postMult(mat);
}

void rsMatrix::scale(float x, float y, float z) {
	rsMatrix mat;
	mat.makeScale(x, y, z);
	this->postMult(mat);
}

void rsMatrix::scale(const float* s) {
	rsMatrix mat;
	mat.makeScale(s);
	this->postMult(mat);
}

void rsMatrix::scale(const rsVec &vec) {
	rsMatrix mat;
	mat.makeScale(vec);
	this->postMult(mat);
}


void rsMatrix::rotate(float a, float x, float y, float z) {
	rsMatrix mat;
	mat.makeRotate(a, x, y, z);
	this->postMult(mat);
}


void rsMatrix::rotate(float a, const rsVec &v) {
	rsMatrix mat;
	mat.makeRotate(a, v);
	this->postMult(mat);
}


void rsMatrix::rotate(const rsQuat &q) {
	rsMatrix mat;
	mat.makeRotate(q);
	this->postMult(mat);
}



float rsMatrix::determinant() const {
	return determinant4(m);
}


bool rsMatrix::invert(const rsMatrix &mat) {
	return invertMatrix(mat.m, m);
}


void rsMatrix::transpose(const rsMatrix &mat) {
	this->m[ 0] = mat[ 0];
	this->m[ 1] = mat[ 4];
	this->m[ 2] = mat[ 8];
	this->m[ 3] = mat[12];
	this->m[ 4] = mat[ 1];
	this->m[ 5] = mat[ 5];
	this->m[ 6] = mat[ 9];
	this->m[ 7] = mat[13];
	this->m[ 8] = mat[ 2];
	this->m[ 9] = mat[ 6];
	this->m[10] = mat[10];
	this->m[11] = mat[14];
	this->m[12] = mat[ 3];
	this->m[13] = mat[ 7];
	this->m[14] = mat[11];
	this->m[15] = mat[15];
}

void rsMatrix::rotationInvert(const rsMatrix &mat){
	float det = mat[0] * mat[5] * mat[10]
		+ mat[4] * mat[9] * mat[2]
		+ mat[8] * mat[1] * mat[6]
		- mat[2] * mat[5] * mat[8]
		- mat[6] * mat[9] * mat[0]
		- mat[10] * mat[1] * mat[4];

	m[0] = (mat[5] * mat[10] - mat[6] * mat[9]) / det;
	m[1] = (mat[6] * mat[8] - mat[4] * mat[10]) / det;
	m[2] = (mat[4] * mat[9] - mat[5] * mat[8]) / det;
	m[4] = (mat[9] * mat[2] - mat[10] * mat[1]) / det;
	m[5] = (mat[10] * mat[0] - mat[8] * mat[2]) / det;
	m[6] = (mat[8] * mat[1] - mat[9] * mat[0]) / det;
	m[8] = (mat[1] * mat[6] - mat[2] * mat[5]) / det;
	m[9] = (mat[2] * mat[4] - mat[0] * mat[6]) / det;
	m[10] = (mat[0] * mat[5] - mat[1] * mat[4]) / det;
	m[3] = m[7] = m[11] = m[12] = m[13] = m[14] = 0.0f;
	m[15] = 1.0f;
}


void rsMatrix::fromQuat(const rsQuat &q){
	float s, xs, ys, zs, wx, wy, wz, xx, xy, xz, yy, yz, zz;

	// must have an axis
	if(q[0] == 0.0f && q[1] == 0.0f && q[2] == 0.0f){
		identity();
		return;
	}

	s = 2.0f / (q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);
	xs = q[0] * s;
	ys = q[1] * s;
	zs = q[2] * s;
	wx = q[3] * xs;
	wy = q[3] * ys;
	wz = q[3] * zs;
	xx = q[0] * xs;
	xy = q[0] * ys;
	xz = q[0] * zs;
	yy = q[1] * ys;
	yz = q[1] * zs;
	zz = q[2] * zs;

	m[0] = 1.0f - yy - zz;
	m[1] = xy + wz;
	m[2] = xz - wy;
	m[3] = 0.0f;
	m[4] = xy - wz;
	m[5] = 1.0f - xx - zz;
	m[6] = yz + wx;
	m[7] = 0.0f;
	m[8] = xz + wy;
	m[9] = yz - wx;
	m[10] = 1.0f - xx - yy;
	m[11] = 0.0f;
	m[12] = 0.0f;
	m[13] = 0.0f;
	m[14] = 0.0f;
	m[15] = 1.0f;
}


rsVec rsMatrix::transPoint(const rsVec &p) const {
	return rsVec(
		p[0] * m[0] + p[1] * m[4] + p[2] * m[ 8] + m[12],
		p[0] * m[1] + p[1] * m[5] + p[2] * m[ 9] + m[13],
		p[0] * m[2] + p[1] * m[6] + p[2] * m[10] + m[14] );
}


rsVec rsMatrix::transVec(const rsVec &v) const {
	return rsVec(
		v[0] * m[0] + v[1] * m[4] + v[2] * m[ 8],
		v[0] * m[1] + v[1] * m[5] + v[2] * m[ 9],
		v[0] * m[2] + v[1] * m[6] + v[2] * m[10] );
}


std::ostream & rsMatrix::operator << (std::ostream &os) const {
	return os 
		<< "| " << m[0] << " " << m[4] << " " << m[8] << " " << m[12] << " |" << std::endl
		<< "| " << m[1] << " " << m[5] << " " << m[9] << " " << m[13] << " |" << std::endl
		<< "| " << m[2] << " " << m[6] << " " << m[10] << " " << m[14] << " |" << std::endl
		<< "| " << m[3] << " " << m[7] << " " << m[11] << " " << m[15] << " |" << std::endl;
}
