/*
 * Copyright (C) 2005-2010  Terence M. Welsh
 *
 * This file is part of Hyperspace.
 *
 * Hyperspace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * Hyperspace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "splinePath.h"
#include <rsMath/rsMath.h>
#include <math.h>



splinePath::splinePath(int length){
	int i;

	step = 0.0f;

	numPoints = length;
	// 6 is the minimum number of points necessary for a tunnel to have one segment
	if(numPoints < 6)
		numPoints = 6;

	phase.resize(numPoints);
	rate.resize(numPoints);
	movevec.resize(numPoints);
	basexyz.resize(numPoints);
	xyz.resize(numPoints);
	basedir.resize(numPoints);
	dir.resize(numPoints);

	basexyz[numPoints - 1].set();
	basexyz[numPoints - 2].set(0.0f, 0.0f, 4.0f);

	for(i=0; i<numPoints; i++)
		makeNewPoint();
}


splinePath::~splinePath(){
	xyz.clear();
}


void splinePath::moveAlongPath(float increment){
	step += increment;
	while(step >= 1.0f){
		step -= 1.0f;
		makeNewPoint();
	}
}


// "section" indicates the pair of points you're between (from 1 to numPoints-1)
// "where" indicates where you are between that pair of points (0.0 - 1.0)
// "position" receives the information about the point you want
void splinePath::getPoint(int section, float where, rsVec& position) const {
	if(section < 1)
		section = 1;
	if(section > numPoints - 3)
		section = numPoints - 3;

	position.interpolate(
		xyz[section - 1], 
		xyz[section],
		xyz[section + 1], 
		xyz[section + 2], 
		where);
}


// "section" indicates the pair of points you're between (from 1 to numPoints-1)
// "where" indicates where you are between that pair of points (0.0 - 1.0)
// "direction" receives the information about the direction you want
void splinePath::getDirection(int section, float where, rsVec& direction) const {
	if(section < 1)
		section = 1;
	if(section > numPoints - 3)
		section = numPoints - 3;

	direction.interpolate(dir[section-1], dir[section],
		dir[section+1], dir[section+2], where);

	direction.normalize();
}


// "section" indicates the pair of points you're between (from 1 to numPoints-1)
// "where" indicates where you are between that pair of points (0.0 - 1.0)
// "direction" receives the information about the direction you want
void splinePath::getBaseDirection(int section, float where, rsVec& direction) const {
	if(section < 1)
		section = 1;
	if(section > numPoints - 3)
		section = numPoints - 3;

	direction.interpolate(basedir[section-1], basedir[section],
		basedir[section+1], basedir[section+2], where);

	direction.normalize();
}


void splinePath::update(float multiplier){
	int i;

	// calculate xyz positions
	for(i=0; i<numPoints; i++){
		phase[i] += rate[i] * multiplier;
		xyz[i] = basexyz[i] + movevec[i] * cosf(phase[i]);
	}

	// calculate direction vectors
	for(i=1; i<numPoints-1; i++){
		dir[i] = xyz[i+1] - xyz[i-1];
	}
}


void splinePath::makeNewPoint(){
	int i;

	// shift points to rear of path
	for(i=0; i<numPoints-1; i++){
		basexyz[i] = basexyz[i+1];
		movevec[i] = movevec[i+1];
		xyz[i] = xyz[i+1];
		phase[i] = phase[i+1];
		rate[i] = rate[i+1];
	}

	// make vector to new point
	int lastPoint = numPoints - 1;
	float tempx = basexyz[lastPoint-1][0] - basexyz[lastPoint-2][0];
	float tempz = basexyz[lastPoint-1][2] - basexyz[lastPoint-2][2];

	// find good angle
	float turnAngle;
	const float pathAngle = atan2f(tempx, tempz);
	const float dist_from_center = sqrtf(basexyz[lastPoint][0] * basexyz[lastPoint][0] + basexyz[lastPoint][2] * basexyz[lastPoint][2]);
	if(dist_from_center > 100.0f){
		const float angleToCenter = atan2f(-basexyz[lastPoint][0], -basexyz[lastPoint][2]);
		turnAngle = angleToCenter - pathAngle;
		if(turnAngle > RS_PI)
			turnAngle -= RS_PIx2;
		if(turnAngle < -RS_PI)
			turnAngle += RS_PIx2;
		if(turnAngle > 0.7f)
			turnAngle = 0.7f;
		if(turnAngle < -0.7f)
			turnAngle = -0.7f;
	}
	else
		turnAngle = rsRandf(1.4f) - 0.7f;

	// rotate new point to some new position
	float ca = cosf(turnAngle);
	float sa = sinf(turnAngle);
	basexyz[lastPoint][0] = tempx * ca + tempz * sa;
	basexyz[lastPoint][1] = 0.0f;
	basexyz[lastPoint][2] = tempx * -sa + tempz * ca;

	// normalize and set length of vector
	// make it at least length 2, which is the grid size of the goo
	float lengthener = (rsRandf(6.0f) + 2.0f) / sqrtf(basexyz[lastPoint][0] * basexyz[lastPoint][0]
		+ basexyz[lastPoint][2] * basexyz[lastPoint][2]);
	basexyz[lastPoint][0] *= lengthener;
	basexyz[lastPoint][2] *= lengthener;

	// make new movement vector proportional to base vector
	movevec[lastPoint][0] = rsRandf(0.25f) * -basexyz[lastPoint][2];
	movevec[lastPoint][1] = 0.3f;
	movevec[lastPoint][2] = rsRandf(0.25f) * basexyz[lastPoint][0];

	// add vector to previous point to get new point
	basexyz[lastPoint][0] += basexyz[lastPoint-1][0];
	basexyz[lastPoint][2] += basexyz[lastPoint-1][2];

	// make new phase and movement rate
	phase[lastPoint] = rsRandf(RS_PIx2);
	rate[lastPoint] = rsRandf(1.0f);

	// reset base direction vectors
	for(i=1; i<numPoints-2; i++){
		basedir[i] = basexyz[i + 1] - basexyz[i - 1];
	}
}

