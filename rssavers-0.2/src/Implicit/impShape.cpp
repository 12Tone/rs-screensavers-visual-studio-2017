/*
 * Copyright (C) 2001-2010  Terence M. Welsh
 *
 * This file is part of Implicit.
 *
 * Implicit is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Implicit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <Implicit/impShape.h>
#include <string.h>


impShape::impShape() {
	mat.identity();
	invmat.identity();

	thickness = 0.1f;
	thicknessSquared = thickness * thickness;
}


void impShape::setPosition(float x, float y, float z) {
	mat[12] = x;
	mat[13] = y;
	mat[14] = z;
	invmat[12] = -x;
	invmat[13] = -y;
	invmat[14] = -z;
	invtrmat[3] = -x;
	invtrmat[7] = -y;
	invtrmat[11] = -z;
#ifdef __SSE__
	((float*)(&(invtrmatrow[0])))[3] = -x;
	((float*)(&(invtrmatrow[1])))[3] = -y;
	((float*)(&(invtrmatrow[2])))[3] = -z;
#endif
}


// Don't need to set this for simple spheres.
// A whole matrix is only necessary for weird asymmetric objects.
void impShape::setMatrix(const float* m) {

	mat.set(m);
	
	invmat.invert(mat);
	invtrmat.transpose(invmat);

#ifdef __SSE__
	invtrmatrow[0] = _mm_loadu_ps(invtrmat);
	invtrmatrow[1] = _mm_loadu_ps(&(invtrmat[4]));
	invtrmatrow[2] = _mm_loadu_ps(&(invtrmat[8]));
	invtrmatrow[3] = _mm_loadu_ps(&(invtrmat[12]));
#endif
}


float impShape::value(const float* position) const {
	return(0.0f);
}


void impShape::getCenter(float* position) const {
	position[0] = mat[12];
	position[1] = mat[13];
	position[2] = mat[14];
}


void impShape::addCrawlPoint(impCrawlPointVector &cpv) const {
	cpv.push_back(impCrawlPoint(mat.getTranslate()));
}
