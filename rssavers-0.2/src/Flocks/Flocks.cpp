/*
 * Copyright (C) 1999-2010  Terence M. Welsh
 *
 * This file is part of Flocks.
 *
 * Flocks is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * Flocks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


// Flocks screensaver

#ifdef WIN32
#include <windows.h>
#include <rsWin32Saver/rsWin32Saver.h>
#include <time.h>
#include <regstr.h>
#include <commctrl.h>
#include <resource.h>
#endif
#ifdef RS_XSCREENSAVER
#include <rsXScreenSaver/rsXScreenSaver.h>
#endif

#include <stdio.h>
#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <rsText/rsText.h>
#include <rsMath/rsMath.h>
#include <rsMath/rsVec.h>
#include <Rgbhsl/Rgbhsl.h>


class bug;


// Globals
#ifdef WIN32
LPCTSTR registryPath = ("Software\\Really Slick\\Flocks");
HDC hdc;
HGLRC hglrc;
#endif
int readyToDraw = 0;
float frameTime = 0.0f;
float aspectRatio;
int wide;
int high;
int deep;
std::vector<bug> lBugs;
std::vector<bug> fBugs;
float colorFade;
// text output
rsText textwriter;
// Parameters edited in the dialog box
int dLeaders;
int dFollowers;
int dGeometry;
int dSize;
int dComplexity;
int dSpeed;
int dStretch;
int dColorfadespeed;
int dChromatek;
int dConnections;


class bug {
public:
	int isLeader;  // 0 = leader   1 = follower
	rsVec hsl;
	rsVec rgb, rgbHalf;
	rsVec pos;
	rsVec velocity;
	float maxSpeed;
	float accel;
	int right, up, forward;
	int leader;
	float craziness;  // How prone to switching direction is this leader
	float nextChange;  // Time until this leader's next direction change

	bug();
	virtual ~bug();
	void initLeader();
	void initFollower();
	void update(const std::vector<bug>& bugs);
};

bug::bug() {
}

bug::~bug(){
}

void bug::initLeader(){
	isLeader = 0;
	hsl.set(rsRandf(1.0), 1.0, 1.0);
	pos.set(
		rsRandf(float(wide * 2)) - float(wide),
		rsRandf(float(high * 2)) - float(high),
		rsRandf(float(wide * 2)) + float(wide * 2) );
	right = up = forward = 1;
	velocity.set(0, 0, 0);
	maxSpeed = 8.0f * float(dSpeed);
	accel = 13.0f * float(dSpeed);
	craziness = rsRandf(4.0f) + 0.05f;
	nextChange = 1.0f;
}

void bug::initFollower(){
	isLeader = 1;
	hsl.set(rsRandf(1.0), 1.0, 1.0);
	pos.set(
		rsRandf(float(wide * 2)) - float(wide),
		rsRandf(float(high * 2)) - float(high),
		rsRandf(float(wide * 5)) + float(wide * 2) );
	right = up = forward = 0;
	velocity.set(0, 0, 0);
	maxSpeed = (rsRandf(6.0f) + 4.0f) * float(dSpeed);
	accel = (rsRandf(4.0f) + 9.0f) * float(dSpeed);
	leader = 0;
}

void bug::update(const std::vector<bug>& bugs){
	int i;
	rsVec scale;
	float scale3;

	if(!isLeader){  // leader
		nextChange -= frameTime;
		if(nextChange <= 0.0f){
			if(rsRandi(2))
				right ++;
			if(rsRandi(2))
				up ++;
			if(rsRandi(2))
				forward ++;
			if(right >= 2)
				right = 0;
			if(up >= 2)
				up = 0;
			if(forward >= 2)
				forward = 0;
			nextChange = rsRandf(craziness);
		}
		if(right)
			velocity[0] += accel * frameTime;
		else
			velocity[0] -= accel * frameTime;
		if(up)
			velocity[1] += accel * frameTime;
		else
			velocity[1] -= accel * frameTime;
		if(forward)
			velocity[2] -= accel * frameTime;
		else
			velocity[2] += accel * frameTime;
		if(pos[0] < float(-wide))
			right = 1;
		if(pos[0] > float(wide))
			right = 0;
		if(pos[1] < float(-high))
			up = 1;
		if(pos[1] > float(high))
			up = 0;
		if(pos[2] < float(-deep))
			forward = 0;
		if(pos[2] > float(deep))
			forward = 1;
		// Even leaders change color from Chromatek 3D
		if(dChromatek) {
			hsl[0] = 0.666667f * ((float(wide) - pos[2]) / float(wide + wide));
			hsl[0] = CLAMP(hsl[0], 0.0f, 0.666667f);
		}
	}
	else{  // follower
		if(!rsRandi(10)) {
			float oldDistance = 10000000.0f, newDistance;
			for(i=0; i<dLeaders; i++){
				newDistance = (bugs[i].pos - this->pos).lengthsq();
				if(newDistance < oldDistance){
					oldDistance = newDistance;
					leader = i;
				}
			}
		}
		rsVec delta = bugs[leader].pos - this->pos;
		if(delta[0] > 0.0f)
			velocity[0] += accel * frameTime;
		else
			velocity[0] -= accel * frameTime;
		if(delta[1] > 0.0f)
			velocity[1] += accel * frameTime;
		else
			velocity[1] -= accel * frameTime;
		if(delta[2] > 0.0f)
			velocity[2] += accel * frameTime;
		else
			velocity[2] -= accel * frameTime;
		if(dChromatek){
			hsl[0] = 0.666667f * ((float(wide) - pos[2]) / float(wide + wide));
			hsl[0] = CLAMP(hsl[0], 0.0f, 0.666667f);
		}
		else{
			float thisHue = hsl[0], leaderHue = bugs[leader].hsl[0];
			float hueDiff = fabs(thisHue - leaderHue);

			if (hueDiff < (colorFade * frameTime)) {
				hsl[0] = leaderHue;
			}
			else{
				if(hueDiff < 0.5f) {
					if(thisHue > leaderHue)
						thisHue -= colorFade * frameTime;
					else
						thisHue += colorFade * frameTime;
				}
				else{
					if(thisHue > leaderHue)
						thisHue += colorFade * frameTime;
					else
						thisHue -= colorFade * frameTime;
					if(thisHue > 1.0f)
						thisHue -= 1.0f;
					else if(thisHue < 0.0f)
						thisHue += 1.0f;
				}
				hsl[0] = thisHue;
			}
		}
	}

	velocity.clamp(-maxSpeed, maxSpeed);
	
	pos += velocity * frameTime;
	if(dStretch){
		scale = velocity * 0.04f;
		scale3 = scale.lengthsq();
		if(scale3 > 0.0f){
			scale3 = sqrt(scale3);
			scale /= scale3;
		}
	}
	hsl2rgb(hsl, rgb);
	rgbHalf = 0.5 * rgb;
	glColor3fv(rgb);
	if(dGeometry){  // Draw blobs
		glPushMatrix();
			glTranslatef(pos[0], pos[1], pos[2]);
			if(dStretch){
				scale3 *= float(dStretch) * 0.05f;
				if(scale3 < 1.0f)
					scale3 = 1.0f;
				glRotatef(float(atan2(-scale[0], -scale[2])) * RS_RAD2DEG, 0.0f, 1.0f, 0.0f);
				glRotatef(float(asin(scale[1])) * RS_RAD2DEG, 1.0f, 0.0f, 0.0f);
				glScalef(1.0f, 1.0f, scale3);
			}
			glCallList(1);
		glPopMatrix();
	}
	else{  // Draw dots
		if(dStretch){
			glLineWidth(float(dSize) * float(700 - pos[2]) * 0.001f);
			scale *= float(dStretch);
			glBegin(GL_LINES);
				glVertex3fv(pos - scale);
				glVertex3fv(pos + scale);
			glEnd();
		}
		else{
			glPointSize(float(dSize) * float(700 - pos[2]) * 0.001f);
			glBegin(GL_POINTS);
				glVertex3fv(pos);
			glEnd();
		}
	}

	if(dConnections && isLeader){  // draw connections
		glLineWidth(1.0f);
		glBegin(GL_LINES);
			glColor3fv(rgbHalf);
			glVertex3fv(pos);
			glColor3fv(bugs[leader].rgbHalf);
			glVertex3fv(bugs[leader].pos);
		glEnd();
	}
}


void draw(){
	int i;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -float(wide * 2));

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Update and draw leaders
	for(i=0; i<dLeaders; i++)
		lBugs[i].update(lBugs);
	// Update and draw followers
	for(i=0; i<dFollowers; i++)
		fBugs[i].update(lBugs);

	// print text
	static float totalTime = 0.0f;
	totalTime += frameTime;
	static std::string str;
	static int frames = 0;
	++frames;
	if(frames == 20){
		str = "FPS = " + to_string(20.0f / totalTime);
		totalTime = 0.0f;
		frames = 0;
	}
	if(kStatistics){
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0.0f, 50.0f * aspectRatio, 0.0f, 50.0f, -1.0f, 1.0f);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();
		glTranslatef(1.0f, 48.0f, 0.0f);

		glColor3f(1.0f, 0.6f, 0.0f);
		textwriter.draw(str);

		glPopMatrix();
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
	}

#ifdef WIN32
	wglSwapLayerBuffers(hdc, WGL_SWAP_MAIN_PLANE);
#endif
#ifdef RS_XSCREENSAVER
	glXSwapBuffers(xdisplay, xwindow);
#endif
}


void idleProc(){
	// update time
	static rsTimer timer;
	frameTime = timer.tick();

	if(readyToDraw && !isSuspended && !checkingPassword)
		draw();
}


void setDefaults(){
	dLeaders = 4;
	dFollowers = 1000;
	dGeometry = 1;
	dSize = 5;
	dComplexity = 1;
	dSpeed = 15;
	dStretch = 20;
	dColorfadespeed = 15;
	dChromatek = 0;
	dConnections = 0;
}


#ifdef RS_XSCREENSAVER
void handleCommandLine(int argc, char* argv[]){
	setDefaults();
	getArgumentsValue(argc, argv, std::string("-leaders"), dLeaders, 1, 100);
	getArgumentsValue(argc, argv, std::string("-followers"), dFollowers, 0, 10000);
	getArgumentsValue(argc, argv, std::string("-geometry"), dGeometry, 0, 1);
	getArgumentsValue(argc, argv, std::string("-size"), dSize, 1, 100);
	getArgumentsValue(argc, argv, std::string("-complexity"), dComplexity, 1, 10);
	getArgumentsValue(argc, argv, std::string("-speed"), dSpeed, 1, 100);
	getArgumentsValue(argc, argv, std::string("-stretch"), dStretch, 0, 100);
	getArgumentsValue(argc, argv, std::string("-colorfadespeed"), dColorfadespeed, 0, 100);
	getArgumentsValue(argc, argv, std::string("-fadespeed"), dColorfadespeed, 0, 100);
	getArgumentsValue(argc, argv, std::string("-chromatek"), dChromatek, 0, 1);
	getArgumentsValue(argc, argv, std::string("-connections"), dConnections, 0, 1);
}
#endif


void reshape(int width, int height){
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	aspectRatio = float(width) / float(height);
	gluPerspective(50.0, aspectRatio, 0.1, 2000.0);
	glMatrixMode(GL_MODELVIEW);
	
	// calculate boundaries
	if(aspectRatio >= 1.0f){
		high = deep = 160;
		wide = int(float(high) * aspectRatio);
	}
	else{
		wide = deep = 160;
		high = int(float(wide) / aspectRatio);
	}
}


#ifdef WIN32
void initSaver(HWND hwnd){
	RECT rect;

	// Window initialization
	hdc = GetDC(hwnd);
	setBestPixelFormat(hdc);
	hglrc = wglCreateContext(hdc);
	GetClientRect(hwnd, &rect);
	wglMakeCurrent(hdc, hglrc);
	
	reshape(rect.right, rect.bottom);
#endif
#ifdef RS_XSCREENSAVER
void initSaver(){
#endif
	int i;

	srand((unsigned)time(NULL));

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	if(dGeometry){  // Setup lights and build blobs
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		float ambient[4] = {0.25f, 0.25f, 0.25f, 0.0f};
		float diffuse[4] = {1.0f, 1.0f, 1.0f, 0.0f};
		float specular[4] = {1.0f, 1.0f, 1.0f, 0.0f};
		float position[4] = {500.0f, 500.0f, 500.0f, 0.0f};
		glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
		glLightfv(GL_LIGHT0, GL_POSITION, position);
		glEnable(GL_COLOR_MATERIAL);
		glMaterialf(GL_FRONT, GL_SHININESS, 10.0f);
		glColorMaterial(GL_FRONT, GL_SPECULAR);
		glColor3f(0.7f, 0.7f, 0.7f);
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

		glNewList(1, GL_COMPILE);
			GLUquadricObj *qobj = gluNewQuadric();
			gluSphere(qobj, float(dSize) * 0.5f, dComplexity + 2, dComplexity + 1);
			gluDeleteQuadric(qobj);
		glEndList();
	}
	else{
		if(dStretch == 0){
			// make GL_POINTS round instead of square
			glEnable(GL_POINT_SMOOTH);
			glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		}
	}

	lBugs.resize(dLeaders);
	fBugs.resize(dFollowers);
	for(i=0; i<dLeaders; i++)
		lBugs[i].initLeader();
	for(i=0; i<dFollowers; i++)
		fBugs[i].initFollower();

	colorFade = float(dColorfadespeed) * 0.01f;
	
	readyToDraw = 1;
}


#ifdef RS_XSCREENSAVER
void cleanUp(){
	// Free memory
	lBugs.clear();
	fBugs.clear();
}
#endif


#ifdef WIN32
void cleanUp(HWND hwnd){
	// Free memory
	lBugs.clear();
	fBugs.clear();

	// Kill device context
	ReleaseDC(hwnd, hdc);
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hglrc);
}


// Initialize all user-defined stuff
void readRegistry(){
	LONG result;
	HKEY skey;
	DWORD valtype, valsize, val;

	setDefaults();

	result = RegOpenKeyEx(HKEY_CURRENT_USER, registryPath, 0, KEY_READ, &skey);
	if(result != ERROR_SUCCESS)
		return;

	valsize=sizeof(val);

	result = RegQueryValueEx(skey, "Leaders", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dLeaders = val;
	result = RegQueryValueEx(skey, "Followers", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dFollowers = val;
	result = RegQueryValueEx(skey, "Geometry", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dGeometry = val;
	result = RegQueryValueEx(skey, "Size", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dSize = val;
	result = RegQueryValueEx(skey, "Complexity", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dComplexity = val;
	result = RegQueryValueEx(skey, "Speed", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dSpeed = val;
	result = RegQueryValueEx(skey, "Stretch", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dStretch = val;
	result = RegQueryValueEx(skey, "Colorfadespeed", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dColorfadespeed = val;
	result = RegQueryValueEx(skey, "Chromatek", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dChromatek = val;
	result = RegQueryValueEx(skey, "Connections", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dConnections = val;
	result = RegQueryValueEx(skey, "FrameRateLimit", 0, &valtype, (LPBYTE)&val, &valsize);
	if(result == ERROR_SUCCESS)
		dFrameRateLimit = val;

	RegCloseKey(skey);
}


// Save all user-defined stuff
void writeRegistry(){
    LONG result;
	HKEY skey;
	DWORD val, disp;

	result = RegCreateKeyEx(HKEY_CURRENT_USER, registryPath, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &skey, &disp);
	if(result != ERROR_SUCCESS)
		return;

	val = dLeaders;
	RegSetValueEx(skey, "Leaders", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dFollowers;
	RegSetValueEx(skey, "Followers", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dGeometry;
	RegSetValueEx(skey, "Geometry", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dSize;
	RegSetValueEx(skey, "Size", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dComplexity;
	RegSetValueEx(skey, "Complexity", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dSpeed;
	RegSetValueEx(skey, "Speed", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dStretch;
	RegSetValueEx(skey, "Stretch", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dColorfadespeed;
	RegSetValueEx(skey, "Colorfadespeed", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dChromatek;
	RegSetValueEx(skey, "Chromatek", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dConnections;
	RegSetValueEx(skey, "Connections", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));
	val = dFrameRateLimit;
	RegSetValueEx(skey, "FrameRateLimit", 0, REG_DWORD, (CONST BYTE*)&val, sizeof(val));

	RegCloseKey(skey);
}


BOOL CALLBACK aboutProc(HWND hdlg, UINT msg, WPARAM wpm, LPARAM lpm){
	switch(msg){
	case WM_CTLCOLORSTATIC:
		if((HWND(lpm) == GetDlgItem(hdlg, WEBPAGE)) || (HWND(lpm) == GetDlgItem(hdlg, CHROMATEKWEBPAGE))){
			SetTextColor(HDC(wpm), RGB(0,0,255));
			SetBkColor(HDC(wpm), COLORREF(GetSysColor(COLOR_3DFACE)));
			return int(GetSysColorBrush(COLOR_3DFACE));
		}
		break;
    case WM_COMMAND:
		switch(LOWORD(wpm)){
		case IDOK:
		case IDCANCEL:
			EndDialog(hdlg, LOWORD(wpm));
			break;
		case WEBPAGE:
			ShellExecute(NULL, "open", "http://www.reallyslick.com", NULL, NULL, SW_SHOWNORMAL);
			break;
		case CHROMATEKWEBPAGE:
			ShellExecute(NULL, "open", "http://www.chromatek.com", NULL, NULL, SW_SHOWNORMAL);
		}
	}
	return FALSE;
}


void initControls(HWND hdlg){

	SendDlgItemMessage(hdlg, LEADERS, UDM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(100), DWORD(1))));
	SendDlgItemMessage(hdlg, LEADERS, UDM_SETPOS, 0, LPARAM(dLeaders));

	SendDlgItemMessage(hdlg, FOLLOWERS, UDM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(10000), DWORD(0))));
	SendDlgItemMessage(hdlg, FOLLOWERS, UDM_SETPOS, 0, LPARAM(dFollowers));

	SendDlgItemMessage(hdlg, GEOMETRY, CB_DELETESTRING, WPARAM(1), 0);
	SendDlgItemMessage(hdlg, GEOMETRY, CB_DELETESTRING, WPARAM(0), 0);
	SendDlgItemMessage(hdlg, GEOMETRY, CB_ADDSTRING, 0, LPARAM("Dots"));
	SendDlgItemMessage(hdlg, GEOMETRY, CB_ADDSTRING, 0, LPARAM("Blobs"));
	SendDlgItemMessage(hdlg, GEOMETRY, CB_SETCURSEL, WPARAM(dGeometry), 0);

	SendDlgItemMessage(hdlg, SIZE, TBM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(1), DWORD(100))));
	SendDlgItemMessage(hdlg, SIZE, TBM_SETPOS, 1, LPARAM(dSize));
	SendDlgItemMessage(hdlg, SIZE, TBM_SETLINESIZE, 0, LPARAM(1));
	SendDlgItemMessage(hdlg, SIZE, TBM_SETPAGESIZE, 0, LPARAM(10));
	SetDlgItemFormatText(hdlg, SIZETEXT, dSize);

	SendDlgItemMessage(hdlg, COMPLEXITY, TBM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(1), DWORD(10))));
	SendDlgItemMessage(hdlg, COMPLEXITY, TBM_SETPOS, 1, LPARAM(dComplexity));
	SendDlgItemMessage(hdlg, COMPLEXITY, TBM_SETLINESIZE, 0, LPARAM(1));
	SendDlgItemMessage(hdlg, COMPLEXITY, TBM_SETPAGESIZE, 0, LPARAM(2));
	SetDlgItemFormatText(hdlg, COMPLEXITYTEXT, dComplexity);
	if(dGeometry)
		EnableWindow(GetDlgItem(hdlg, COMPLEXITY), TRUE);
	else
		EnableWindow(GetDlgItem(hdlg, COMPLEXITY), FALSE);

	SendDlgItemMessage(hdlg, SPEED, TBM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(1), DWORD(100))));
	SendDlgItemMessage(hdlg, SPEED, TBM_SETPOS, 1, LPARAM(dSpeed));
	SendDlgItemMessage(hdlg, SPEED, TBM_SETLINESIZE, 0, LPARAM(1));
	SendDlgItemMessage(hdlg, SPEED, TBM_SETPAGESIZE, 0, LPARAM(10));
	SetDlgItemFormatText(hdlg, SPEEDTEXT, dSpeed);

	SendDlgItemMessage(hdlg, STRETCH, TBM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(0), DWORD(100))));
	SendDlgItemMessage(hdlg, STRETCH, TBM_SETPOS, 1, LPARAM(dStretch));
	SendDlgItemMessage(hdlg, STRETCH, TBM_SETLINESIZE, 0, LPARAM(1));
	SendDlgItemMessage(hdlg, STRETCH, TBM_SETPAGESIZE, 0, LPARAM(10));
	SetDlgItemFormatText(hdlg, STRETCHTEXT, dStretch);

	SendDlgItemMessage(hdlg, COLORFADE, TBM_SETRANGE, 0, LPARAM(MAKELONG(DWORD(0), DWORD(100))));
	SendDlgItemMessage(hdlg, COLORFADE, TBM_SETPOS, 1, LPARAM(dColorfadespeed));
	SendDlgItemMessage(hdlg, COLORFADE, TBM_SETLINESIZE, 0, LPARAM(1));
	SendDlgItemMessage(hdlg, COLORFADE, TBM_SETPAGESIZE, 0, LPARAM(10));
	SetDlgItemFormatText(hdlg, COLORFADETEXT, dColorfadespeed);

	CheckDlgButton(hdlg, CHROMATEK, dChromatek);

	CheckDlgButton(hdlg, CONNECTIONS, dConnections);

	initFrameRateLimitSlider(hdlg, FRAMERATELIMIT, FRAMERATELIMITTEXT);
}


BOOL screenSaverConfigureDialog(HWND hdlg, UINT msg,
										 WPARAM wpm, LPARAM lpm){
	int ival;

    switch(msg){
    case WM_INITDIALOG:
        InitCommonControls();
        readRegistry();
        initControls(hdlg);
        return TRUE;
    case WM_COMMAND:
        switch(LOWORD(wpm)){
        case IDOK:
            dLeaders = SendDlgItemMessage(hdlg, LEADERS, UDM_GETPOS, 0, 0);
			dFollowers = SendDlgItemMessage(hdlg, FOLLOWERS, UDM_GETPOS, 0, 0);
			dGeometry = SendDlgItemMessage(hdlg, GEOMETRY, CB_GETCURSEL, 0, 0);
			dSize = SendDlgItemMessage(hdlg, SIZE, TBM_GETPOS, 0, 0);
			dComplexity = SendDlgItemMessage(hdlg, COMPLEXITY, TBM_GETPOS, 0, 0);
			dSpeed = SendDlgItemMessage(hdlg, SPEED, TBM_GETPOS, 0, 0);
			dStretch = SendDlgItemMessage(hdlg, STRETCH, TBM_GETPOS, 0, 0);
			dColorfadespeed = SendDlgItemMessage(hdlg, COLORFADE, TBM_GETPOS, 0, 0);
			dChromatek = (IsDlgButtonChecked(hdlg, CHROMATEK) == BST_CHECKED);
			dConnections = (IsDlgButtonChecked(hdlg, CONNECTIONS) == BST_CHECKED);
			dFrameRateLimit = SendDlgItemMessage(hdlg, FRAMERATELIMIT, TBM_GETPOS, 0, 0);
			writeRegistry();
            // Fall through
        case IDCANCEL:
            EndDialog(hdlg, LOWORD(wpm));
            break;
		case DEFAULTS:
			setDefaults();
			initControls(hdlg);
			break;
        case ABOUT:
			DialogBox(mainInstance, MAKEINTRESOURCE(DLG_ABOUT), hdlg, DLGPROC(aboutProc));
			break;
		case GEOMETRY:
			if(SendDlgItemMessage(hdlg, GEOMETRY, CB_GETCURSEL, 0, 0) == 1)
				EnableWindow(GetDlgItem(hdlg, COMPLEXITY), TRUE);
			else
				EnableWindow(GetDlgItem(hdlg, COMPLEXITY), FALSE);
		}
        return TRUE;
	case WM_HSCROLL:
		if(HWND(lpm) == GetDlgItem(hdlg, SIZE)){
			ival = SendDlgItemMessage(hdlg, SIZE, TBM_GETPOS, 0, 0);
			SetDlgItemFormatText(hdlg, SIZETEXT, ival);
		}
		if(HWND(lpm) == GetDlgItem(hdlg, COMPLEXITY)){
			ival = SendDlgItemMessage(hdlg, COMPLEXITY, TBM_GETPOS, 0, 0);
			SetDlgItemFormatText(hdlg, COMPLEXITYTEXT, ival);
		}
		if(HWND(lpm) == GetDlgItem(hdlg, SPEED)){
			ival = SendDlgItemMessage(hdlg, SPEED, TBM_GETPOS, 0, 0);
			SetDlgItemFormatText(hdlg, SPEEDTEXT, ival);
		}
		if(HWND(lpm) == GetDlgItem(hdlg, STRETCH)){
			ival = SendDlgItemMessage(hdlg, STRETCH, TBM_GETPOS, 0, 0);
			SetDlgItemFormatText(hdlg, STRETCHTEXT, ival);
		}
		if(HWND(lpm) == GetDlgItem(hdlg, COLORFADE)){
			ival = SendDlgItemMessage(hdlg, COLORFADE, TBM_GETPOS, 0, 0);
			SetDlgItemFormatText(hdlg, COLORFADETEXT, ival);
		}
		if(HWND(lpm) == GetDlgItem(hdlg, FRAMERATELIMIT))
			updateFrameRateLimitSlider(hdlg, FRAMERATELIMIT, FRAMERATELIMITTEXT);
		return TRUE;
    }
    return FALSE;
}


LONG screenSaverProc(HWND hwnd, UINT msg, WPARAM wpm, LPARAM lpm){
	static unsigned long threadID;

	switch(msg){
	case WM_CREATE:
		readRegistry();
		initSaver(hwnd);
		break;
	case WM_DESTROY:
		readyToDraw = 0;
		cleanUp(hwnd);
		break;
	}
	return defScreenSaverProc(hwnd, msg, wpm, lpm);
}
#endif // WIN32
